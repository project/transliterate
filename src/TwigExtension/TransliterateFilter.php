<?php

namespace Drupal\transliterate\TwigExtension;

use Drupal\Component\Transliteration\PhpTransliteration;


class TransliterateFilter extends \Twig_Extension {

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('transliterate', array($this, 'transliterateString')),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'transliterate.twig_extension';
  }

  /**
   * Transliterates a string
   */
  public static function transliterateString($string) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $trans = new PHPTransliteration();
    return $trans->transliterate($string, $langcode);
  }

}
